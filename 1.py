import http.client
import json
import time
import csv
import random

if __name__ == "__main__":

    currentPage = 1
    totalPage = 1
    while True:
        print("Processing page: ", currentPage)
        conn = http.client.HTTPSConnection("app.apollo.io")
        payload = json.dumps({
            "finder_view_id": "5a205be19a57e40c095e1d5f",
            "organization_ids": [
                "54a11ee569702d97c111c301"
            ],
            "page": currentPage,
            "view_mode": "table",
            "display_mode": "explorer_mode",
            "per_page": 100,
            "open_factor_names": [],
            "num_fetch_result": 4,
            "context": "people-index-page",
            "show_suggestions": False,
            "include_contact_engagement_stats": False,
            "include_account_engagement_stats": False,
            "ui_finder_random_seed": "smhdm6207g",
            "cacheKey": 1637400281031
        })
        headers = {
            'x-csrf-token': 'zUujR7VCzyPqTjE6vwAClLjfzTPS5d8+Fxv6Cet8YTWlxvCTwy29W7a0K//9YHs8k8GftMAlu3OM1KMcxXdhZg==',
            'Cookie': 'zp__initial_utm_medium=cpc; zp__initial_utm_source=google; zp__utm_campaign=gg_br-in_dg_acquisition_performancemax; zp__initial_utm_campaign=gg_br-in_dg_acquisition_performancemax; _gac_UA-63770898-6=1.1636990432.CjwKCAiAp8iMBhAqEiwAJb94z3onG19F80veTaoaQLkFFq8qfBsQ8LNc3YAJR9pco3Il6UTWe8l7oBoCpE8QAvD_BwE; _gcl_au=1.1.1942651857.1636990443; _fbp=fb.1.1636990444035.1650802968; _ym_uid=163699044467787708; _ym_d=1636990444; zp__utm_medium=Discovery; _cioanonid=5f69b95f-2766-4293-b8fa-d1f8df61500a; __stripe_mid=47a84c7a-e4f2-4879-b0c3-16a58b6e3b8c603743; zp__utm_source=app.apollo.io; ZP_Pricing_Split_Test_Variant=20Q2_A49; drift_aid=d6fd7076-3bb1-47ff-90e0-627ff8f7ba75; driftt_aid=d6fd7076-3bb1-47ff-90e0-627ff8f7ba75; remember_token_leadgenie=BAhJIj42MTk0YjJjMmQ4MTI1NzAwZjcyOGY0ZTVfNDIyODQ3MzUzMjUxOGVkMGIyMDU2NGExY2EyNWRlNzIGOgZFVA%3D%3D--c3aa7438091fe377934e55712d6a5c58bd135222; app_token=7288b04cfb8e40fd10fdfe5c714f400e; _cioid=6194b2c2d8125700f728f4e5; drift_eid=6194b2c2d8125700f728f4e5; _ce.s=v11.rlc~1637862526003; _gid=GA1.2.1873235927.1638008555; _ga=GA1.1.50562527.1636990432; _uetsid=f0492f804f6b11ecb4a1d5d7d7d70534; _uetvid=76a72100462911ecab462bff2b9ad19e; _ym_visorc=w; _ym_isad=2; GCLB=CK21vMOzzoK63QE; _ga_76XXTC73SP=GS1.1.1638008554.8.0.1638008558.0; drift_campaign_refresh=744de295-72a2-4c03-a2fa-6b78e432bd60; __stripe_sid=d3f433ab-4ec2-4cd5-89fa-74e01f226bc1d346b0; amplitude_id_122a93c7d9753d2fe678deffe8fac4cfapollo.io=eyJkZXZpY2VJZCI6IjViMTkxOWU5LTk4NzEtNGQ2Ni05MDU5LTQyODBiYmI0ZWUyZVIiLCJ1c2VySWQiOiI2MTk0YjJjMmQ4MTI1NzAwZjcyOGY0ZTUiLCJvcHRPdXQiOmZhbHNlLCJzZXNzaW9uSWQiOjE2MzgwMDg1NTQ2NzAsImxhc3RFdmVudFRpbWUiOjE2MzgwMDg2MTk2OTYsImV2ZW50SWQiOjY0LCJpZGVudGlmeUlkIjo0MCwic2VxdWVuY2VOdW1iZXIiOjEwNH0=; X-CSRF-TOKEN=zUujR7VCzyPqTjE6vwAClLjfzTPS5d8%2BFxv6Cet8YTWlxvCTwy29W7a0K%2F%2F9YHs8k8GftMAlu3OM1KMcxXdhZg%3D%3D; _leadgenie_session=c1RLczhvWjBkZ2IvaVVKTURheVp1NXNhOXowdE5EaC83MDVMODN6UmIzWldNL3Qrb3NWK3d2cmg1dityRmJvdllhclRGeCsrQkhMNCs1UGQ3RHF3MkNSUVNOZDdjcTBKSEVYRDc2eDljV2pMTmwrWHZLSEEreVFydEF4Q1M0K01XOTg1dDBINFVadkN1cmZjK1lwOEN5Q2hhTEk4bXo0Y2pmTENtdkxpbFh1Uzh1aUEvRnU4MUttSEFFZUtoOUtILS1mZzhZdzMxOHF1b25BTFVlUFBMTTBRPT0%3D--901ad72257f7b20e00df7cd85b602b87444d6237',
            'Content-Type': 'application/json'
        }
        conn.request("POST", "/api/v1/mixed_people/search", payload, headers)
        res = conn.getresponse()
        data = res.read()
        if(res.code == 200):
            response_data = json.loads(data.decode("utf-8"))
            # print(data.decode("utf-8"))
            dict_keys = empolyee = {
                        'first_name': 1,
                        'last_name': 1,
                        'linkedin_url': 1,
                        'title': 1,
                        'headline': 1,
                        'state': 1,
                        'photo_url': 1,
                        'city': 1,
                        'country': 1,
                        'org_name': 1,
                        'org_id': 1,
                        'org_linkedin_url': 1,
                        'org_website_url': 1,
                        'org_logo_url': 1,
                        'org_founded_year': 1,

                    }
            with open("employee.csv", "w") as outfile:
                w = csv.DictWriter(outfile, dict_keys.keys(), delimiter=',',
                lineterminator='\r\n',quoting=csv.QUOTE_ALL,
                quotechar = "'")
                w.writeheader()

                for record in response_data['people']:
                    empolyee = {
                        'first_name': record.get('first_name'),
                        'last_name': record.get('last_name'),
                        'linkedin_url': record.get('linkedin_url'),
                        'title': record.get('title'),
                        'headline': record.get('headline'),
                        'state': record.get('state'),
                        'photo_url': record.get('photo_url'),
                        'city': record.get('city'),
                        'country': record.get('country')

                    }

                    if( 'organization' in record):
                        empolyee['org_name']= record.get('organization').get('name')
                        empolyee['org_id']= record.get('organization').get('id')
                        empolyee['org_linkedin_url']= record.get('organization').get('linkedin_url')
                        empolyee['org_website_url']= record.get('organization').get('website_url')
                        empolyee['org_logo_url']= record.get('organization').get('logo_url')
                        empolyee['org_founded_year']= record.get('organization').get('founded_year')

                    w.writerow(empolyee)

            sleep_time = random.randint(0, 10) / 100
            print("Sleeping for: ", sleep_time)
            time.sleep(sleep_time)
            #print(response_data)
            print("Response data received:", len(response_data['people']))
            totalPage = response_data['pagination']['total_pages']
            print("total_pages:",totalPage)
        else:
            print("Error occurred while processing the records :" + data)
            break

        if(currentPage == totalPage):
            print("Completed all pages")
            break
        currentPage = currentPage + 1
