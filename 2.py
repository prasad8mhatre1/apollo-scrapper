import http.client
import json
import time
import csv
import random

if __name__ == "__main__":
    conn = http.client.HTTPSConnection("app.apollo.io")
    payload = json.dumps({
        "ids": [
            "5fca408962ba9b00f6d3c961",
            "54a1998674686949e7a2b400",
            "5f435719789e1300becc23b0",
            "559226da736964191e149b00",
            "5d33d0d1a3ae6113e19b1674",
            "54a1202769702d9a8b734e02",
            "54a1233869702d8ed4ee5703",
            "5ed41777efb6a9008c84a47a",
            "5569b066736964254ff97b00",
            "61825531b3165c00da25af14",
            "5d09312ca3ae61489386b467",
            "5e5ead12972df301604baf5a",
            "54a1215e69702d77c2eee302",
            "57c4d9dea6da9869f38f20af",
            "5f2a39cb77a7440112460cf5",
            "5f48e4944c270e0001e2bb7f",
            "5fc8b5680229240001fcf8a3",
            "5fcd2cf3ed78c700f9383e4e",
            "6182e5452241880182531854",
            "5f4673254768080001bbd31c",
            "5fc93db64c38d300d6aa24e6",
            "6199e9bd5215e800f6518c95"
        ],
        "cacheKey": 1638011298496
    })
    headers = {
        'x-csrf-token': 'vMDvezrXz3yIcwG4D59Ax9uh8l2+1f3bOBCHrMgks0LUTbyvTLi9BNSJG31N/zlv8L+g2qwVmZaj39655i+zEQ==',
        'cookie': 'zp__initial_utm_medium=cpc; zp__initial_utm_source=google; zp__utm_campaign=gg_br-in_dg_acquisition_performancemax; zp__initial_utm_campaign=gg_br-in_dg_acquisition_performancemax; _gac_UA-63770898-6=1.1636990432.CjwKCAiAp8iMBhAqEiwAJb94z3onG19F80veTaoaQLkFFq8qfBsQ8LNc3YAJR9pco3Il6UTWe8l7oBoCpE8QAvD_BwE; _gcl_au=1.1.1942651857.1636990443; _fbp=fb.1.1636990444035.1650802968; _ym_uid=163699044467787708; _ym_d=1636990444; zp__utm_medium=Discovery; _cioanonid=5f69b95f-2766-4293-b8fa-d1f8df61500a; __stripe_mid=47a84c7a-e4f2-4879-b0c3-16a58b6e3b8c603743; zp__utm_source=app.apollo.io; ZP_Pricing_Split_Test_Variant=20Q2_A49; drift_aid=d6fd7076-3bb1-47ff-90e0-627ff8f7ba75; driftt_aid=d6fd7076-3bb1-47ff-90e0-627ff8f7ba75; remember_token_leadgenie=BAhJIj42MTk0YjJjMmQ4MTI1NzAwZjcyOGY0ZTVfNDIyODQ3MzUzMjUxOGVkMGIyMDU2NGExY2EyNWRlNzIGOgZFVA%3D%3D--c3aa7438091fe377934e55712d6a5c58bd135222; app_token=7288b04cfb8e40fd10fdfe5c714f400e; _cioid=6194b2c2d8125700f728f4e5; drift_eid=6194b2c2d8125700f728f4e5; _ce.s=v11.rlc~1637862526003; _gid=GA1.2.1873235927.1638008555; _ga=GA1.1.50562527.1636990432; _uetsid=f0492f804f6b11ecb4a1d5d7d7d70534; _uetvid=76a72100462911ecab462bff2b9ad19e; _ym_isad=2; _ga_76XXTC73SP=GS1.1.1638008554.8.0.1638008558.0; GCLB=CLCKsdylpqKVBw; __stripe_sid=d3f433ab-4ec2-4cd5-89fa-74e01f226bc1d346b0; amplitude_id_122a93c7d9753d2fe678deffe8fac4cfapollo.io=eyJkZXZpY2VJZCI6IjViMTkxOWU5LTk4NzEtNGQ2Ni05MDU5LTQyODBiYmI0ZWUyZVIiLCJ1c2VySWQiOiI2MTk0YjJjMmQ4MTI1NzAwZjcyOGY0ZTUiLCJvcHRPdXQiOmZhbHNlLCJzZXNzaW9uSWQiOjE2MzgwMTEyNzcwMjUsImxhc3RFdmVudFRpbWUiOjE2MzgwMTEyOTQzMDYsImV2ZW50SWQiOjY4LCJpZGVudGlmeUlkIjo0MCwic2VxdWVuY2VOdW1iZXIiOjEwOH0=; X-CSRF-TOKEN=vMDvezrXz3yIcwG4D59Ax9uh8l2%2B1f3bOBCHrMgks0LUTbyvTLi9BNSJG31N%2Fzlv8L%2Bg2qwVmZaj39655i%2BzEQ%3D%3D; _leadgenie_session=UDFMUjUyTFpBcXU3R3ZGbWZmOTBsamdqcERWVjZtNEZwdlp6YmZSSS9Ba0FJSE94WjZ0cEN1bGpqN2lxNkxUMys5eDJIUm1OVnNlNG5uTlh1S1lDWHJiZ1pUWkxUVjNoS050TEtoK2lYUXpmR3d5b3hzbGVFYit3UU5KUkRmSm1WdUlYZVNnRmp4NUNXL2FydjcvU2VFeDhWeTd0SlJodXVXTlB3VDBJQmxwOW0zUGNaZGxoUGxBdVRWS0ZQcUxYLS1OZ0V1Vm1JTG9XSHhIb0NLMWU1K2tBPT0%3D--8991b2a5e78f6885fc1be2b1a14614697c119353; X-CSRF-TOKEN=c4ZSnhejGZRMHjtW2HhYyf3KNkn7dG8VU7X5pEER4IQbCwFKYcxr7BDkIZOaGCFh1tRkzum0C1jIeqCxbxrg1w%3D%3D; _leadgenie_session=REs1L3VYb3Y5b3RQaUhWUDFiZUpubVhVOTZBamg3MzRVTEZEQTVhT3I4SExBRUhVdi9MN2tLOS81aUp6RFVrNDVYVU40dUVaR2p4OE44YXExNE0xMDFHOUlPZjhhcG9pOXk2bE5hYU1pZlU5ZzI3ZGZTV3N3ZnUzekE0UGJaejA0aG1LcUgwcFRzNXVFSFRCRFpROU5KT0tYZ2t4ek1oekRtbVovUDgxTGdLUlBUR0ZLakw3RklIU1BXanJBUTJRLS1yQS81L1RpdmI5MGN0Tkk5ZVpmRzBRPT0%3D--dc81014705f0adf47a25163ef110caffbec7729f',
        'Content-Type': 'application/json'
    }
    conn.request("POST", "/api/v1/organizations/load_snippets", payload, headers)
    res = conn.getresponse()
    data = res.read()
    print(data.decode("utf-8"))
